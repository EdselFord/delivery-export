<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id_delivery
 * @property string $sale_code
 * @property string $paid_approve
 * @property string $note
 * @property Date   $sale_date
 * @property Date   $paid_date
 * @property float  $income
 * @property int    $created_at
 * @property int    $updated_at
 * @property int    $deleted_at
 */
class Sales extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_delivery', 'sale_code', 'sale_date', 'paid_status', 'paid_date', 'paid_approve', 'income', 'note', 'confirm_status', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_delivery' => 'string', 'sale_code' => 'string', 'sale_date' => 'date', 'paid_date' => 'date', 'paid_approve' => 'string', 'income' => 'double', 'note' => 'string', 'created_at' => 'timestamp', 'updated_at' => 'timestamp', 'deleted_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'sale_date', 'paid_date', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...

    public function items() {
        return $this->hasMany(SaleItems::class, 'id_sale');
    }
}
