<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property Date $date
 * @property int  $unique_visitor_day
 * @property int  $visitor_day
 * @property int  $created_at
 * @property int  $updated_at
 */
class ShortLinkTrafficts extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'short_link_trafficts';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date', 'unique_visitor_day', 'visitor_day', 'short_links_id', 'domain_decentralizes_id', 'created_at', 'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'date' => 'date', 'unique_visitor_day' => 'int', 'visitor_day' => 'int', 'created_at' => 'timestamp', 'updated_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'date', 'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...
}
