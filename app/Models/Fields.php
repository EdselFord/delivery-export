<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 * @property string $jenis_tanaman
 * @property string $google_embed_code
 * @property string $link_to_redirect
 * @property string $name
 * @property string $plant_type
 * @property string $google_embed_code
 * @property string $link_to_redirect
 * @property float  $luas
 * @property float  $length
 * @property float  $width
 * @property float  $land_area
 * @property int    $created_at
 * @property int    $updated_at
 * @property int    $created_at
 * @property int    $updated_at
 */
class Fields extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'fields';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'jenis_tanaman', 'luas', 'google_embed_code', 'link_to_redirect', 'created_at', 'updated_at', 'name', 'plant_type', 'field_type', 'length', 'width', 'land_area', 'google_embed_code', 'link_to_redirect', 'created_at', 'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string', 'jenis_tanaman' => 'string', 'luas' => 'double', 'google_embed_code' => 'string', 'link_to_redirect' => 'string', 'created_at' => 'timestamp', 'updated_at' => 'timestamp', 'name' => 'string', 'plant_type' => 'string', 'length' => 'double', 'width' => 'double', 'land_area' => 'double', 'google_embed_code' => 'string', 'link_to_redirect' => 'string', 'created_at' => 'timestamp', 'updated_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...
}
