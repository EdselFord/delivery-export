<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property Date   $date_purchase
 * @property string $title
 * @property string $note
 * @property string $id_merchant
 * @property int    $created_at
 * @property int    $updated_at
 * @property int    $deleted_at
 */
class FlowerPurchases extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'flower_purchases';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date_purchase', 'title', 'note', 'id_merchant', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'date_purchase' => 'date', 'title' => 'string', 'note' => 'string', 'id_merchant' => 'string', 'created_at' => 'timestamp', 'updated_at' => 'timestamp', 'deleted_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'date_purchase', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...
}
