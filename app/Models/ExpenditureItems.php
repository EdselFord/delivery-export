<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int    $id_expenditure
 * @property int    $created_at
 * @property int    $updated_at
 * @property int    $created_at
 * @property int    $updated_at
 * @property string $name
 * @property string $unit
 * @property string $id_expend
 * @property string $name
 * @property string $qty_unit
 * @property float  $qty
 * @property float  $price
 * @property float  $amount
 * @property float  $qty
 * @property float  $price
 * @property float  $amount
 */
class ExpenditureItems extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'expenditure_items';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_expenditure', 'name', 'qty', 'unit', 'price', 'amount', 'created_at', 'updated_at', 'id_expend', 'name', 'qty', 'qty_unit', 'price', 'amount', 'created_at', 'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_expenditure' => 'int', 'name' => 'string', 'qty' => 'double', 'unit' => 'string', 'price' => 'double', 'amount' => 'double', 'created_at' => 'timestamp', 'updated_at' => 'timestamp', 'id_expend' => 'string', 'name' => 'string', 'qty' => 'double', 'qty_unit' => 'string', 'price' => 'double', 'amount' => 'double', 'created_at' => 'timestamp', 'updated_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...
}
