<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int  $employee_id
 * @property int  $total
 * @property int  $created_at
 * @property int  $updated_at
 * @property Date $from_date
 * @property Date $to_date
 */
class Salaries extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'salaries';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id', 'from_date', 'to_date', 'total', 'created_at', 'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'employee_id' => 'int', 'from_date' => 'date', 'to_date' => 'date', 'total' => 'int', 'created_at' => 'timestamp', 'updated_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'from_date', 'to_date', 'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...
}
