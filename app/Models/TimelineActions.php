<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $blocks
 * @property string $action_name
 * @property string $working_hours
 * @property string $material
 * @property string $material_unit
 * @property string $result_unit
 * @property int    $worker
 * @property int    $full_day_male
 * @property int    $full_day_female
 * @property int    $half_day_male
 * @property int    $half_day_female
 * @property int    $created_at
 * @property int    $updated_at
 * @property float  $worker_cost
 * @property float  $full_day_cost_male
 * @property float  $full_day_cost_female
 * @property float  $half_day_cost_male
 * @property float  $half_day_cost_female
 * @property float  $material_qty
 * @property float  $result
 */
class TimelineActions extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'timeline_actions';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_timeline', 'blocks', 'action_name', 'worker', 'worker_cost', 'working_hours', 'full_day_male', 'full_day_cost_male', 'full_day_female', 'full_day_cost_female', 'half_day_male', 'half_day_cost_male', 'half_day_female', 'half_day_cost_female', 'material', 'material_qty', 'material_unit', 'result', 'result_unit', 'created_at', 'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'blocks' => 'string', 'action_name' => 'string', 'worker' => 'int', 'worker_cost' => 'double', 'working_hours' => 'string', 'full_day_male' => 'int', 'full_day_cost_male' => 'double', 'full_day_female' => 'int', 'full_day_cost_female' => 'double', 'half_day_male' => 'int', 'half_day_cost_male' => 'double', 'half_day_female' => 'int', 'half_day_cost_female' => 'double', 'material' => 'string', 'material_qty' => 'double', 'material_unit' => 'string', 'result' => 'double', 'result_unit' => 'string', 'created_at' => 'timestamp', 'updated_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...
}
