<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $campaign_name
 * @property string $description
 * @property string $short_url
 * @property string $original_url
 * @property string $meta
 * @property int    $created_at
 * @property int    $updated_at
 */
class ShortLinks extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'short_links';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'campaign_name', 'description', 'short_url', 'original_url', 'user_id', 'meta', 'created_at', 'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'campaign_name' => 'string', 'description' => 'string', 'short_url' => 'string', 'original_url' => 'string', 'meta' => 'string', 'created_at' => 'timestamp', 'updated_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...
}
