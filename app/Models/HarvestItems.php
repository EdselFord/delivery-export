<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id_harvest
 * @property string $id_flower
 * @property int    $qty
 * @property int    $created_at
 * @property int    $updated_at
 * @property int    $deleted_at
 * @property Date   $harvest_date
 */
class HarvestItems extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'harvest_items';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_harvest', 'id_flower', 'qty', 'harvest_date', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_harvest' => 'string', 'id_flower' => 'string', 'qty' => 'int', 'harvest_date' => 'date', 'created_at' => 'timestamp', 'updated_at' => 'timestamp', 'deleted_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'harvest_date', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...
}
