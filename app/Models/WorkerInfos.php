<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int   $full_day_worker
 * @property int   $half_day_worker
 * @property int   $created_at
 * @property int   $updated_at
 * @property float $full_day_worker_cost
 * @property float $half_day_worker_cost
 */
class WorkerInfos extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'worker_infos';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_timeline_action', 'full_day_worker', 'full_day_worker_cost', 'half_day_worker', 'half_day_worker_cost', 'gender', 'created_at', 'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'full_day_worker' => 'int', 'full_day_worker_cost' => 'double', 'half_day_worker' => 'int', 'half_day_worker_cost' => 'double', 'created_at' => 'timestamp', 'updated_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...
}
