<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property Date   $delivery_date
 * @property string $id_delivery
 * @property string $id_flower
 * @property string $note
 * @property int    $qty
 * @property int    $created_at
 * @property int    $updated_at
 * @property int    $deleted_at
 * @property float  $price
 */
class DeliveryItems extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'delivery_items';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'delivery_date', 'id_delivery', 'id_flower', 'qty', 'price', 'flower_origin', 'note', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'delivery_date' => 'date', 'id_delivery' => 'string', 'id_flower' => 'string', 'qty' => 'int', 'price' => 'double', 'note' => 'string', 'created_at' => 'timestamp', 'updated_at' => 'timestamp', 'deleted_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'delivery_date', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...


    function flower() {
        return $this->belongsTo(Flowers::class, 'id_flower');
    }
}
