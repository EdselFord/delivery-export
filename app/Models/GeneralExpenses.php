<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int    $id_timeline_action
 * @property int    $id_field
 * @property int    $created_at
 * @property int    $updated_at
 * @property int    $deleted_at
 * @property string $label
 * @property string $unit
 * @property float  $qty
 * @property float  $price
 */
class GeneralExpenses extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'general_expenses';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_timeline_action', 'id_field', 'label', 'qty', 'unit', 'price', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_timeline_action' => 'int', 'id_field' => 'int', 'label' => 'string', 'qty' => 'double', 'unit' => 'string', 'price' => 'double', 'created_at' => 'timestamp', 'updated_at' => 'timestamp', 'deleted_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...
}
