<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property Date   $harvest_date
 * @property Date   $harvest_date
 * @property int    $quantity
 * @property int    $created_at
 * @property int    $updated_at
 * @property int    $created_at
 * @property int    $updated_at
 * @property int    $deleted_at
 * @property string $unit
 * @property string $description
 * @property string $id_field
 * @property string $note
 */
class Harvests extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'harvests';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'harvest_date', 'plants_id', 'quantity', 'unit', 'selling_price', 'total_income', 'fields_id', 'description', 'created_at', 'updated_at', 'harvest_date', 'id_field', 'note', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'harvest_date' => 'date', 'quantity' => 'int', 'unit' => 'string', 'description' => 'string', 'created_at' => 'timestamp', 'updated_at' => 'timestamp', 'harvest_date' => 'date', 'id_field' => 'string', 'note' => 'string', 'created_at' => 'timestamp', 'updated_at' => 'timestamp', 'deleted_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'harvest_date', 'created_at', 'updated_at', 'harvest_date', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...
}
