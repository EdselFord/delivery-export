<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int   $id_timeline_action
 * @property int   $id_field
 * @property int   $total_worker
 * @property int   $working_hour
 * @property int   $created_at
 * @property int   $updated_at
 * @property int   $deleted_at
 * @property Date  $date
 * @property float $price_per_worker
 * @property float $total_price
 */
class FieldWorkers extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'field_workers';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_timeline_action', 'id_field', 'date', 'total_worker', 'gender', 'working_hour', 'price_per_worker', 'total_price', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_timeline_action' => 'int', 'id_field' => 'int', 'date' => 'date', 'total_worker' => 'int', 'working_hour' => 'int', 'price_per_worker' => 'double', 'total_price' => 'double', 'created_at' => 'timestamp', 'updated_at' => 'timestamp', 'deleted_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'date', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...
}
