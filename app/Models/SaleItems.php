<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id_sale
 * @property string $id_flower
 * @property int    $qty_delivery
 * @property int    $qty
 * @property int    $retur_qty
 * @property int    $created_at
 * @property int    $updated_at
 * @property float  $price
 * @property float  $loss_price
 * @property float  $total_price
 */
class SaleItems extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sale_items';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_sale', 'id_flower', 'flower_origin', 'qty_delivery', 'qty', 'price', 'retur_qty', 'loss_price', 'total_price', 'created_at', 'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_sale' => 'string', 'id_flower' => 'string', 'qty_delivery' => 'int', 'qty' => 'int', 'price' => 'double', 'retur_qty' => 'int', 'loss_price' => 'double', 'total_price' => 'double', 'created_at' => 'timestamp', 'updated_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...

    public function flower() {
        return $this->belongsTo(Flowers::class, 'id_flower');
    }
}
