<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $delivery_code
 * @property string $id_flower_purchase
 * @property string $destination
 * @property string $note
 * @property Date   $delivery_date
 * @property int    $koli
 * @property int    $created_at
 * @property int    $updated_at
 * @property int    $deleted_at
 * @property float  $ongkir_amount
 * @property float  $estimated_income
 */
class Deliveries extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'deliveries';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'delivery_code', 'id_flower_purchase', 'delivery_date', 'destination', 'koli', 'with_ongkir', 'ongkir_amount', 'estimated_income', 'note', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'delivery_code' => 'string', 'id_flower_purchase' => 'string', 'delivery_date' => 'date', 'destination' => 'string', 'koli' => 'int', 'ongkir_amount' => 'double', 'estimated_income' => 'double', 'note' => 'string', 'created_at' => 'timestamp', 'updated_at' => 'timestamp', 'deleted_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'delivery_date', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...

    public function sales() {
        return $this->hasOne(Sales::class, 'id_delivery', 'id');
    }

    public function items() {
        return $this->hasMany(DeliveryItems::class, 'id_delivery');
    }

    public function merchant() {
        return $this->belongsTo(Merchants::class, 'destination');
    }
}
