<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 * @property string $email
 * @property string $username
 * @property string $password
 * @property string $remember_token
 * @property string $name
 * @property string $username
 * @property string $user_label
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $merchant_location
 * @property string $remember_token
 * @property int    $email_verified_at
 * @property int    $created_at
 * @property int    $updated_at
 * @property int    $id_team
 * @property int    $email_verified_at
 * @property int    $created_at
 * @property int    $updated_at
 * @property int    $email_verified_at
 * @property int    $created_at
 * @property int    $updated_at
 * @property int    $deleted_at
 */
class Users extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username', 'email_verified_at', 'password', 'remember_token', 'created_at', 'updated_at', 'id_team', 'name', 'username', 'user_label', 'email', 'email_verified_at', 'password', 'role', 'remember_token', 'created_at', 'updated_at', 'name', 'email', 'email_verified_at', 'password', 'as_merchant', 'merchant_location', 'remember_token', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string', 'email' => 'string', 'username' => 'string', 'email_verified_at' => 'timestamp', 'password' => 'string', 'remember_token' => 'string', 'created_at' => 'timestamp', 'updated_at' => 'timestamp', 'id_team' => 'int', 'name' => 'string', 'username' => 'string', 'user_label' => 'string', 'email' => 'string', 'email_verified_at' => 'timestamp', 'password' => 'string', 'remember_token' => 'string', 'created_at' => 'timestamp', 'updated_at' => 'timestamp', 'name' => 'string', 'email' => 'string', 'email_verified_at' => 'timestamp', 'password' => 'string', 'merchant_location' => 'string', 'remember_token' => 'string', 'created_at' => 'timestamp', 'updated_at' => 'timestamp', 'deleted_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'email_verified_at', 'created_at', 'updated_at', 'email_verified_at', 'created_at', 'updated_at', 'email_verified_at', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...
}
