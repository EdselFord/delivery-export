<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id_purchases
 * @property string $item
 * @property string $qty_unit
 * @property Date   $date_purchase
 * @property int    $qty
 * @property int    $created_at
 * @property int    $updated_at
 * @property float  $price
 * @property float  $amount
 */
class FlowerPurchasesItems extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'flower_purchases_items';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_purchases', 'date_purchase', 'item', 'qty', 'qty_unit', 'price', 'amount', 'created_at', 'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_purchases' => 'string', 'date_purchase' => 'date', 'item' => 'string', 'qty' => 'int', 'qty_unit' => 'string', 'price' => 'double', 'amount' => 'double', 'created_at' => 'timestamp', 'updated_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'date_purchase', 'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...
}
