<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;

class SummaryHarvestExport implements FromCollection, WithHeadings, WithEvents
{
    private $collection;
    private $location;

    public function __construct($collection, $location)
    {
        $this->collection = $collection;
        $this->location = $location;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $a = $this->collection->map(function ($f) {
            $arr = collect($f)->slice(1);
            return collect([''])->merge($f)->merge(collect(['min' => $arr->min(), 'max' => $arr->max(), 'average' => $arr->average()]));
        });

        // $a = $this->collection->map(function ($f, $index) {
        //     $arr = collect($f)->slice(1);
        //     $colrow = Coordinate::stringFromColumnIndex(3) . ($index + 4) . ':' . Coordinate::stringFromColumnIndex(12*4+2) . ($index + 4);
        //     return collect([''])->merge($f)->merge(collect(['min' => '=MIN(' . $colrow  . ')', 'max' => '=MAX(' . $colrow  . ')', 'average' => '=AVERAGE(' . $colrow  . ')']));
        // });

        return $a;
    }

    public function headings(): array
    {
        return [
            ['Terjual 2023'],
            [' '],
            array_merge(array_merge([' ', ' '], ...array_fill(0, 12, [...$this->location->map(fn ($loc) => $loc->location ), 'Total', 'Panen'])), ['Min', 'Max', 'Average'])
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $lastRow = $event->sheet->getDelegate()->getHighestDataRow();
                $lastColumn = $event->sheet->getDelegate()->getHighestDataColumn();

                $event->sheet->mergeCells('A1:' . $lastColumn . '1');
                $event->sheet->getStyle("A1:" . $lastColumn . '1')->getFont()->setBold(true);
                $event->sheet->getStyle("A1:" . $lastColumn . '1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $months = ['JAN', 'FEB', 'MAR', 'APR', 'MEI', 'JUN', 'JUL', 'AGS', 'SEP', 'OKT', 'NOV', 'DES'];
                foreach ($months as $index => $month) {
                    $a = Coordinate::stringFromColumnIndex($index * (2 + $this->location->count()) + 3) . '2:' . Coordinate::stringFromColumnIndex($index * (2 + $this->location->count()) + (4 + $this->location->count())) . '2';
                    $event->sheet->setCellValue(Coordinate::stringFromColumnIndex($index * (2 + $this->location->count()) + 3) . '2', $month);
                    $event->sheet->mergeCells($a);
                }

                $event->sheet->setCellValue('A2', 'BULAN');
                $event->sheet->mergeCells('A2:B2');

                $event->sheet->setCellValue('A4', 'NAMA TANAMAN');
                $event->sheet->mergeCells('A4:A' . $lastRow);
                $event->sheet->getStyle('A4:A' . $lastRow)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->getStyle('A4:A' . $lastRow)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getStyle('A4:A' . $lastRow)->getAlignment()->setTextRotation(90);

                $event->sheet->getStyle('A1:' . $lastColumn . $lastRow)->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                ]);
            }
        ];
    }
}
