<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;

class FlowerExport implements FromCollection, WithHeadings, WithEvents
{

    private $deliveries;
    private $count;

    public function __construct($deliveries, $count)
    {
        $this->deliveries = $deliveries;
        $arr = collect($count)->map(fn ($a) => $a->count)->values();
        $this->count = collect(['Jml. Pengiriman'])
        ->merge($arr)
        ->merge(
            collect([
                $arr->min(), 
                $arr->max(), 
                $arr->average()
            ])
        );
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $this->deliveries->map(function ($del) {
            $arr = collect($del)->slice(1);
            return collect([''])->merge($del)->merge(collect(['min' => $arr->min(), 'max' => $arr->max(), 'average' => $arr->average()]));
        });
    }

    public function headings(): array
    {
        return [
            ['Terjual 2023'],
            [
                ' ',
                'Bulan',
                'JAN',
                'FEB',
                'MAR',
                'APR',
                'MEI',
                'JUN',
                'JUL',
                'AGS',
                'SEP',
                'OKT',
                'NOV',
                'DES',
                'Min',
                'Max',
                'Average'
            ],
            [' ']
        ];
    }


    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $lastRow = $event->sheet->getDelegate()->getHighestDataRow();
                $lastColumn = $event->sheet->getDelegate()->getHighestDataColumn();

                $event->sheet->mergeCells('A1:' . $lastColumn . '1');
                $event->sheet->getStyle("A1:" . $lastColumn . '1')->getFont()->setBold(true);
                $event->sheet->getStyle("A1:" . $lastColumn . '1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $this->count->each(function($a, $i) use ($event) {
                    $column = Coordinate::stringFromColumnIndex($i+2);
                    $event->sheet->setCellValue($column.'3', $a);
                });

                $event->sheet->setCellValue('A2', 'BULAN');
                $event->sheet->mergeCells('A2:B2');

                $event->sheet->setCellValue('A3', 'Jml. Pengiriman');
                $event->sheet->mergeCells('A3:B3');

                $event->sheet->setCellValue('A4', 'NAMA TANAMAN');
                $event->sheet->mergeCells('A4:A' . $lastRow);
                $event->sheet->getStyle('A4:A' . $lastRow)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->getStyle('A4:A' . $lastRow)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getStyle('A4:A' . $lastRow)->getAlignment()->setTextRotation(90);

                $event->sheet->getStyle('A1:' . $lastColumn . $lastRow)->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                ]);
            }
        ];
    }
}
