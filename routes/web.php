<?php

use App\Exports\FlowerExport;
use App\Exports\SummaryHarvestExport;
use App\Models\Deliveries;
use App\Models\Flowers;
use App\Models\Merchants;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Maatwebsite\Excel\Facades\Excel;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    $deliveryItemsCount = DB::table('deliveries')
        ->join('merchants', 'merchants.id', '=', 'deliveries.destination')
        ->select(DB::raw('MONTH(deliveries.delivery_date) as month'), DB::raw('COUNT(*) as count'))
        ->whereYear('deliveries.delivery_date', 2023)
        ->where('merchants.location', 'Bali')
        ->groupBy(DB::raw('MONTH(deliveries.delivery_date)'))
        ->orderBy(DB::raw('MONTH(deliveries.delivery_date)'))
        ->get();

    $months = range(1, 12);
    $deliveryItemsCount = collect($months)->map(function ($month) use ($deliveryItemsCount) {
        $item = $deliveryItemsCount->firstWhere('month', $month);
        return $item ? $item : (object) ['month' => $month, 'count' => 0];
    });

    $deliveries = DB::table('deliveries')
        ->join('delivery_items', 'deliveries.id', '=', 'delivery_items.id_delivery')
        ->join('flowers', 'delivery_items.id_flower', '=', 'flowers.id')
        ->join('merchants', 'merchants.id', '=', 'deliveries.destination')
        ->select(
            'flowers.name',
            DB::raw('sum(CASE WHEN MONTH(deliveries.delivery_date) = 1 THEN delivery_items.qty ELSE 0 END) AS JAN'),
            DB::raw('sum(CASE WHEN MONTH(deliveries.delivery_date) = 2 THEN delivery_items.qty ELSE 0 END) AS FEB'),
            DB::raw('sum(CASE WHEN MONTH(deliveries.delivery_date) = 3 THEN delivery_items.qty ELSE 0 END) AS MAR'),
            DB::raw('sum(CASE WHEN MONTH(deliveries.delivery_date) = 4 THEN delivery_items.qty ELSE 0 END) AS APR'),
            DB::raw('sum(CASE WHEN MONTH(deliveries.delivery_date) = 5 THEN delivery_items.qty ELSE 0 END) AS MEI'),
            DB::raw('sum(CASE WHEN MONTH(deliveries.delivery_date) = 6 THEN delivery_items.qty ELSE 0 END) AS JUN'),
            DB::raw('sum(CASE WHEN MONTH(deliveries.delivery_date) = 7 THEN delivery_items.qty ELSE 0 END) AS JUL'),
            DB::raw('sum(CASE WHEN MONTH(deliveries.delivery_date) = 8 THEN delivery_items.qty ELSE 0 END) AS AGS'),
            DB::raw('sum(CASE WHEN MONTH(deliveries.delivery_date) = 9 THEN delivery_items.qty ELSE 0 END) AS SEP'),
            DB::raw('sum(CASE WHEN MONTH(deliveries.delivery_date) = 10 THEN delivery_items.qty ELSE 0 END) AS OKT'),
            DB::raw('sum(CASE WHEN MONTH(deliveries.delivery_date) = 11 THEN delivery_items.qty ELSE 0 END) AS NOV'),
            DB::raw('sum(CASE WHEN MONTH(deliveries.delivery_date) = 12 THEN delivery_items.qty ELSE 0 END) AS DES')
        )
        ->whereYear('deliveries.delivery_date', 2023)
        // ->whereMonth('deliveries.delivery_date', 1)
        // ->whereBetween('deliveries.delivery_date', [DB::raw('DATE_SUB(CURDATE(), INTERVAL 1 YEAR)'), DB::raw('CURDATE()')])
        ->where('merchants.location', 'Bali')
        ->groupBy('flowers.name')
        ->orderBy('flowers.name')
        ->get();

    // dd($deliveryItemsCount);

    return Excel::download(new FlowerExport($deliveries, $deliveryItemsCount), 'pengentidur.xlsx');
});


Route::get('/harvest', function () {
    $location = DB::table('merchants')->select('location')->get();

    $months = collect(['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']);

    $summary = DB::table('flowers')
        ->select(
            'flowers.name',
            ...$months->map(function ($month, $index) use ($location) {
                $cap = ucfirst(strtolower($month));
                return [
                    ...$location->map(function ($loc) use ($index, $month) {
                        return DB::raw('SUM(CASE WHEN merchants.location IN (\''. $loc->location .'\') AND MONTH(deliveries.delivery_date) = ' . $index+1 . ' THEN delivery_items.qty ELSE 0 END) AS ' .$loc->location. '_' . $month);
                    }),
                    DB::raw('SUM(CASE WHEN MONTH(deliveries.delivery_date) = ' . $index+1 . ' THEN delivery_items.qty ELSE 0 END) AS Total_' . $month),
                    DB::raw('\'\' AS Harvest_' . $cap)
                ];
            })->flatten(),
        )
        ->leftJoin('delivery_items', 'delivery_items.id_flower', '=', 'flowers.id')
        ->leftJoin('deliveries', 'deliveries.id', '=', 'delivery_items.id_delivery')
        ->leftJoin('merchants', 'merchants.id', '=', 'deliveries.destination')
        ->whereYear('deliveries.delivery_date', 2023)
        ->orWhereNull('deliveries.delivery_date')
        ->groupBy('flowers.name')
        ->orderBy('flowers.name')
        ->get();


    $harvest = DB::table('flowers')
        ->select(
            'flowers.name',
            ...$months->map(function ($month, $index) {
                $cap = ucfirst(strtolower($month));
                return DB::raw('SUM(CASE WHEN MONTH(harvest_items.harvest_date) = ' . $index+1 . ' THEN harvest_items.qty ELSE 0 END) AS ' . "'$cap'");
            })
        )
        ->leftJoin('harvest_items', 'harvest_items.id_flower', '=', 'flowers.id')
        ->whereYear('harvest_items.harvest_date', 2023)
        ->orWhereNull('harvest_items.harvest_date')
        ->groupBy('flowers.name')
        ->orderBy('flowers.name')
        ->get();

    $summary = $summary->map(function($item) use ($harvest) {
        $col_item = collect($item);
        $harv = collect($harvest->where('name', $item->name)->first())->skip(1);
        $harv->keys()->each(function ($k) use ($col_item, $harv) {
            $col_item["Harvest_$k"] = $harv[$k];
        });
        return $col_item;
    });



    return Excel::download(new SummaryHarvestExport($summary, $location), 'harvest.xlsx');
});
