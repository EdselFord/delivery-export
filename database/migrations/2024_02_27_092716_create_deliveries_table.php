<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->char('id', 36)->primary();
            $table->string('delivery_code');
            $table->string('id_flower_purchase')->nullable();
            $table->date('delivery_date');
            $table->string('destination');
            $table->integer('koli')->default(1);
            $table->enum('with_ongkir', ['no', 'yes'])->default('no');
            $table->double('ongkir_amount')->default(0);
            $table->double('estimated_income')->default(0);
            $table->text('note')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveries');
    }
};
