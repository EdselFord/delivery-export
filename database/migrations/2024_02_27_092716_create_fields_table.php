<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->char('id', 36)->primary();
            $table->string('name');
            $table->longText('plant_type');
            $table->enum('field_type', ['openfield', 'greenhouse'])->default('greenhouse');
            $table->double('length')->default(0);
            $table->double('width')->default(0);
            $table->double('land_area')->default(0);
            $table->text('google_embed_code')->nullable();
            $table->text('link_to_redirect')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fields');
    }
};
