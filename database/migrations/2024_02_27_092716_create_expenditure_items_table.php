<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenditure_items', function (Blueprint $table) {
            $table->char('id', 36)->primary();
            $table->string('id_expend');
            $table->string('name');
            $table->double('qty')->nullable()->default(0);
            $table->string('qty_unit')->nullable();
            $table->double('price')->nullable()->default(0);
            $table->double('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenditure_items');
    }
};
