<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flower_purchases', function (Blueprint $table) {
            $table->char('id', 36)->primary();
            $table->date('date_purchase');
            $table->string('title')->nullable();
            $table->text('note')->nullable();
            $table->string('id_merchant');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flower_purchases');
    }
};
