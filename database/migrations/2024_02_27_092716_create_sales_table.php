<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->char('id', 36)->primary();
            $table->string('id_delivery');
            $table->string('sale_code');
            $table->date('sale_date');
            $table->enum('paid_status', ['paid', 'unpaid'])->default('unpaid');
            $table->date('paid_date')->nullable();
            $table->text('paid_approve')->nullable();
            $table->double('income')->default(0);
            $table->text('note')->nullable();
            $table->enum('confirm_status', ['confirm', 'unconfirm'])->default('unconfirm');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
};
