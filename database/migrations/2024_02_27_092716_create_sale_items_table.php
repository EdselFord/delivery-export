<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_items', function (Blueprint $table) {
            $table->char('id', 36)->primary();
            $table->string('id_sale');
            $table->string('id_flower');
            $table->enum('flower_origin', ['buy', 'own'])->default('own');
            $table->integer('qty_delivery')->default(0);
            $table->integer('qty')->default(0);
            $table->double('price')->default(0);
            $table->integer('retur_qty')->default(0);
            $table->double('loss_price')->default(0);
            $table->double('total_price')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_items');
    }
};
