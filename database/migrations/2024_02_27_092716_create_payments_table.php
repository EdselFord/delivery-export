<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->char('id', 36)->primary();
            $table->string('id_merchant');
            $table->date('payment_date');
            $table->double('amount')->default(0);
            $table->text('paid_approve')->nullable();
            $table->text('note')->nullable();
            $table->enum('status_payment', ['confirm', 'unconfirm', 'invalid'])->default('unconfirm');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
};
