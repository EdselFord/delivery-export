<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flower_purchases_items', function (Blueprint $table) {
            $table->char('id', 36)->primary();
            $table->string('id_purchases');
            $table->date('date_purchase');
            $table->string('item');
            $table->integer('qty')->default(0);
            $table->string('qty_unit')->nullable();
            $table->double('price')->default(0);
            $table->double('amount')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flower_purchases_items');
    }
};
