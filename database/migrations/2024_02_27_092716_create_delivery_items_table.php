<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_items', function (Blueprint $table) {
            $table->char('id', 36)->primary();
            $table->date('delivery_date');
            $table->string('id_delivery');
            $table->string('id_flower');
            $table->integer('qty');
            $table->double('price')->default(0);
            $table->enum('flower_origin', ['own', 'buy'])->default('own');
            $table->string('note')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_items');
    }
};
